# -*- coding: utf-8 -*-

from django.contrib.auth.models import User
from django.db import models

import os, uuid
from datetime import datetime

def get_upload_path(instance, filename):
    return os.path.join('achievements/', '%d' % instance.id, filename)

def get_evidence_upload_path(instance, filename):
    return os.path.join('evidence/', ('%d' % instance.id) if instance != None else '', filename)

def get_evidence_path(achievement, user):
    return os.path.join(os.getcwd(), '%d' % achievement.id, '%d' % user.id)
    
class Category(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField(null = True)
    
    def __unicode__(self):
        return self.name


def make_uuid():
        return str(uuid.uuid4())

class Achievement(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    points = models.IntegerField()
    pub_date = models.DateTimeField('date published', auto_now_add=True)
    author = models.ForeignKey(User, null=True, blank=True)
    progressMax = models.IntegerField(default=0)
    dependencies = models.ManyToManyField("self", null=True, blank=True, symmetrical=False)
    hidden = models.BooleanField(default=False)
    evidenceRequired = models.BooleanField(default=False)
    password = models.CharField(max_length=30, null=True, blank=True)
    image = models.ImageField(upload_to=get_upload_path, null = True, blank=True)
    category = models.ForeignKey(Category, null=True, blank=True)
    deadline = models.DateTimeField('deadline', null=True, blank=True)
    uuid = models.CharField(max_length=36, unique=True, default=make_uuid, editable=False)
    
    def __unicode__(self):
        return self.name
        
        
    @property
    def past_deadline(self):
        if not self.deadline:
            return False
            
        return self.deadline < datetime.now()

        
    @property
    def has_attributes(self):
        return self.password or self.progressMax or self.dependencies.all() or self.deadline
        
        
    @property
    def parse_attributes(self):
        final_str = ""
        
        if self.password:
            final_str += '<span class="label label-important" style="margin-right:5px;">Chránený heslom</span>'
        
        if self.progressMax:
            final_str += '<span class="label label-warning" style="margin-right:5px;">Má priebeh</span>'
    
        if self.dependencies.all():
            final_str += '<span class="label label-important" style="margin-right:5px;">Má závislosti</span>'
            
        if self.deadline:
            final_str += '<span class="label label-warning" style="margin-right:5px;">Má deadline</span>'
            
        if self.hidden:
            final_str += '<span class="label label-inverse" style="margin-right:5px;">Skrytý</span>'
            
        if self.evidenceRequired:
            final_str += '<span class="label label-warning" style="margin-right:5px;">Vyžaduje dôkaz</span>'
            
        return final_str
        

class SubAchievement(models.Model):
    parent = models.ForeignKey(Achievement)
    name = models.CharField(max_length=200)
    description = models.TextField()
    points = models.IntegerField(default=0)

    def __unicode__(self):
        return self.name


class Evidence(models.Model):
    achievement = models.ForeignKey(Achievement)
    user = models.ForeignKey(User)
    description = models.TextField(null = True)
    evidenceFile = models.FileField(upload_to=get_evidence_upload_path, null = True, blank=True)
    
    @property
    def filename(self):
        return os.path.basename(self.evidenceFile.name)
    
    def __unicode__(self):
        return "from " + self.user.username + " for " + self.achievement.name


class Fulfilment(models.Model):
    achievement = models.ForeignKey(Achievement)
    user = models.ForeignKey(User)
    subachievement = models.ManyToManyField(SubAchievement, null=True, blank=True, symmetrical=False)
    date_achieved = models.DateTimeField('date achieved', auto_now_add=True)
    progress = models.IntegerField(default=0)
    evidence = models.OneToOneField(Evidence, null=True, blank=True, on_delete=models.SET_NULL)
    awarded = models.BooleanField(default=False)


