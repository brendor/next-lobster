from django import template

from django.utils import html

from achievements.models import Achievement
from achievements.models import Fulfilment

register = template.Library()

@register.simple_tag
def get_achicatmade_count(usr, cat):
    count = Achievement.objects.filter(author = usr, category__name = cat).count()
    return count
    
@register.simple_tag
def get_achicatdone_count(usr, cat):
    count = Fulfilment.objects.filter(user = usr, awarded = True, achievement__category__name = cat).count()
    return count
    
@register.simple_tag
def get_catpoints(usr, cat):
    fulfiled = Fulfilment.objects.filter(user = usr, awarded = True, achievement__category__name = cat)
    points = 0;
    
    for ful in fulfiled:
	points = points + ful.achievement.points
	
    return points
    
@register.simple_tag
def get_achinocatmade_count(usr):
    count = Achievement.objects.filter(author = usr, category = None).count()
    return count
    
@register.simple_tag
def get_achinocatdone_count(usr):
    count = Fulfilment.objects.filter(user = usr, awarded = True, achievement__category = None).count()
    return count
    
@register.simple_tag
def get_nocatpoints(usr):
    fulfiled = Fulfilment.objects.filter(user = usr, awarded = True, achievement__category = None)
    points = 0;
    
    for ful in fulfiled:
	points = points + ful.achievement.points
	
    return points