from django import template

from django.utils import html

from achievements.models import Achievement

register = template.Library()

SELECT_GLOB_ALL_QUERY = 'SELECT * FROM achievements_achievement WHERE achievements_achievement.hidden != 1'

@register.simple_tag
def get_autofill_data():
    achievement_list = list(Achievement.objects.raw(SELECT_GLOB_ALL_QUERY))
    
    result = ''
    
    if len(achievement_list) > 0:
        for achi in achievement_list:
            result += '"' + html.escape(achi.name) + '",'
        result = result[:-1]
    
    
    return result

@register.simple_tag
def raw_autofill_data():
    achievement_list = list(Achievement.objects.raw(SELECT_GLOB_ALL_QUERY))
    return achievement_list