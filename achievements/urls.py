from django.conf.urls.defaults import *
from django.views.generic import DetailView, ListView

from achievements.models import *

# Uncomment the next two lines to enable the admin:
#from django.contrib import admin
#Sadmin.autodiscover()

urlpatterns = patterns('',
    url(r'^join/(?P<achievement_id>[abcdef\-\d]+)/$', 'achievements.views.join_achievement',  name='join_achievement'),
    url(r'^join/(?P<achievement_id>[abcdef\-\d]+)/sub/(?P<sub_id>\w+)/$', 'achievements.views.join_subachievement',  name='join_subachievement'),
    url(r'^(?P<achievement_id>[abcdef\-\d]+)/$', 'achievements.views.show_achievement',  name='show_one_achievement'),
    url(r'^(?P<achievement_id>[abcdef\-\d]+)/api$', 'achievements.views.achievement_json',  name='achievement_json'),
    url(r'^create/$', 'achievements.views.create_achievement', name='create_achievement'),
)
