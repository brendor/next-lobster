from achievements.models import *
from django.contrib import admin
from django.contrib.auth.models import User

class SubachievementInline(admin.TabularInline):
    model = SubAchievement
    extra = 0

class AchievementsAdmin(admin.ModelAdmin):
    inlines = [SubachievementInline]
    list_display = ('name', 'points', 'pub_date', 'uuid')
    list_filter = ['pub_date']
    search_fields = ['name', 'description']
    date_hierarchy = 'pub_date'
    filter_vertical = ['dependencies']

class SubAchievementAdmin(admin.ModelAdmin):
    list_display = ('name', 'points')
    search_fields = ['name', 'description']

#class FulfilmentInline(admin.TabularInline):
#    model = Fulfilment
#    extra = 0


#class UserProfileAdmin(admin.ModelAdmin):
#    inlines = [FulfilmentInline]


class CommentsAdmin(admin.ModelAdmin):
    inlines = [SubachievementInline]
    list_display = ('user', 'pub_date')
    list_filter = ['pub_date']
    search_fields = ['user', 'description']
    date_hierarchy = 'pub_date'

class FulfilmentAdmin(admin.ModelAdmin):
    list_display = ('user', 'achievement', 'date_achieved', 'evidence')
    filter_vertical = ['subachievement']
    
    
class EvidenceAdmin(admin.ModelAdmin):
    list_display = ('user', 'achievement')

class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'description')
    search_fields = ['name', 'description']
    
admin.site.register(Achievement, AchievementsAdmin)
admin.site.register(Fulfilment, FulfilmentAdmin)
#admin.site.unregister(User)
#admin.site.register(User, UserProfileAdmin)

admin.site.register(SubAchievement, SubAchievementAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Evidence, EvidenceAdmin)
