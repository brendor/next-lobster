# -*- coding: utf-8 -*-

from django.utils import simplejson
from dajaxice.decorators import dajaxice_register

from achievements.models import *
from accounts.models import Messages, UserProfile


@dajaxice_register
def progress_step(request, fulfilment_id, add):
    if request.user.is_authenticated():
        fulist = Fulfilment.objects.filter(id = fulfilment_id, user = request.user)
        if len(list(fulist)) > 0:
            f = fulist[0]
            
            done = False
            
            if f.progress == f.achievement.progressMax:
                done = True
                return simplejson.dumps({'message': fulfilment_id, 'new_progress':100, 'done':done })
            elif not f.awarded:
                if f.progress < f.achievement.progressMax and add:
                    f.progress = f.progress + 1
                elif f.progress > 0 and not add:
                    f.progress = f.progress - 1
                
                f.save()
                
                if f.progress == f.achievement.progressMax:
                    done = True
                    
                    if not f.achievement.evidenceRequired:
                        f.awarded = True
                        request.user.userprofile.user_points += f.achievement.points
                        request.user.userprofile.save()
                
                
                return simplejson.dumps({'message': fulfilment_id, 'new_progress':(float(f.progress)/f.achievement.progressMax)*100, 'done':done })
    
    return simplejson.dumps({'message': fulfilment_id})

@dajaxice_register
def evidence_accept(request, message_id):
    
    if request.user.is_authenticated():
        message_list = Messages.objects.filter(id = message_id)
        
        if len(list(message_list)) > 0:
            m = message_list[0]
            
            if m.message_type == "EM":
                fulf_list = Fulfilment.objects.filter(evidence = m.evidence)
                
                if len(list(fulf_list)) > 0:
                    f = fulf_list[0]
                    if not f.awarded:
                        f.awarded = True
                        f.user.userprofile.user_points += m.evidence.achievement.points
                        
                        f.user.userprofile.save()
                        f.save()
                
                m.evidence.delete()
                m.delete()
                
                return simplejson.dumps({ 'success':True, 'id':message_id })
            
    
    return simplejson.dumps({ 'success':False, 'id':message_id })
    
@dajaxice_register
def evidence_decline(request, message_id):
    
    if request.user.is_authenticated():
        message_list = Messages.objects.filter(id = message_id)
        
        if len(list(message_list)) > 0:
            m = message_list[0]
            
            if m.message_type == "EM":
                fulf_list = Fulfilment.objects.filter(evidence = m.evidence)
                
                if len(list(fulf_list)) > 0:
                    f = fulf_list[0]
                    
                    reply_msg = Messages(recipient = f.user, author = f.achievement.author, message_type = "UM", header = "Žiadosť neprijatá", text = "Vaša žiadosť o splnenie achievementu nebola prijatá.")
                    reply_msg.save()
                    
                    f.delete()
                
                m.evidence.delete()
                m.delete()
                
                return simplejson.dumps({ 'success':True, 'id':message_id })
    
    return simplejson.dumps({ 'success':False, 'id':message_id })