function my_callback(data){
    
    if (data.messages > 0)
    {
        $('#messages,.message_tag').html(data.messages);
        $('#messages,.message_tag').css("display", "inline");
    } else {
        $('#messages,.message_tag').html(data.messages);
        $('#messages,.message_tag').css("display", "none");
    }
}

$(document).ready( function() {
    Dajaxice.accounts.refresh_messages(my_callback);
    setInterval(function() { Dajaxice.accounts.refresh_messages(my_callback); }, 2500);
});