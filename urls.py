from django.conf.urls.defaults import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
from django.conf import settings

admin.autodiscover()

from dajaxice.core import dajaxice_autodiscover, dajaxice_config
dajaxice_autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'views.main_page', name="main_page"),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^achievement/', include('achievements.urls')),
    url(r'^list/$', 'achievements.views.show_tables', name='achievements_search'),
    url(r'^accounts/', include('accounts.urls')),
    url(r'^user/', include('accounts.urls')),
    url(r'^comments/', include('django.contrib.comments.urls')),
    url(r'^files/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    url(dajaxice_config.dajaxice_url, include('dajaxice.urls')),
)
