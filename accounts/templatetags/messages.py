from django import template

from django.utils import html

from accounts.models import Messages

from django.template import Library, Node

register = template.Library()


"""Splits query results list into multiple sublists for template display."""

class SplitListNode(Node):
    def __init__(self, results, cols, new_results):
        self.results, self.cols, self.new_results = results, cols, new_results

    def split_seq(self, results, cols=5):
        new_results = []
        start = 0
        while start < len(results):
            stop = start + cols
            if stop >= len(results):
                new_results += [results[start:]]
                break
            else:
                new_results += [results[start:stop]]
            start = stop
            
        return new_results

    def render(self, context):
        context[self.new_results] = self.split_seq(context[self.results], int(self.cols))
        return ''

def list_to_columns(parser, token):
    """Parse template tag: {% list_to_colums results as new_results 2 %}"""
    bits = token.contents.split()
    if len(bits) != 5:
        raise TemplateSyntaxError, "list_to_columns results as new_results 2"
    if bits[2] != 'as':
        raise TemplateSyntaxError, "second argument to the list_to_columns tag must be 'as'"
    return SplitListNode(bits[1], bits[4], bits[3])
    
list_to_columns = register.tag(list_to_columns)
