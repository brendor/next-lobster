from django.conf.urls.defaults import *
from django.views.generic import DetailView, ListView

from accounts.models import *

urlpatterns = patterns('',
    url(r'^register/$', 'accounts.views.register', name="accounts_register"),
    url(r'^login/$', 'django.contrib.auth.views.login', {'template_name': 'accounts/login.html'}, name="accounts_login"),
    url(r'^logout/$', 'accounts.views.logout_page', name="accounts_logout"),
    url(r'^view/(?P<user_id>\w+)/$', 'accounts.views.show_user', name="show_user"),
    url(r'^view/(?P<user_id>\w+)/api$', 'accounts.views.user_json', name="user_json"),
    url(r'^add/(?P<user_id>\w+)/$', 'accounts.views.add_friend', name="add_friend"),
    url(r'^remove/(?P<user_id>\w+)/$', 'accounts.views.remove_friend', name="remove_friend"),
    url(r'^message/(?P<user_id>\w+)/$', 'accounts.views.message_friend', name="message_friend"),
    url(r'^$', 'accounts.views.show_users_page', name="show_users_page"),
    url(r'^profile/$', 'accounts.views.show_current_user', name="show_current_user"),
)

