from achievements.models import *
from django.contrib import admin

from accounts.models import Messages, UserProfile


class MessagesAdmin(admin.ModelAdmin):
    list_display = ('recipient', 'header', 'text')
    
class UserProfileAdmin(admin.ModelAdmin):
    list_display = ('user', 'user_points')
    filter_vertical = ['friends']
    
    
admin.site.register(Messages, MessagesAdmin)
admin.site.register(UserProfile, UserProfileAdmin)
