from django.utils import simplejson
from dajaxice.decorators import dajaxice_register

from accounts.models import User, UserProfile, Messages
from achievements.models import Achievement, Fulfilment, Category

import types

@dajaxice_register
def refresh_messages(request):
    if request.user.is_authenticated():
        return simplejson.dumps({'messages': str(request.user.userprofile.get_new_messages_count) })
    return simplejson.dumps({'messages': '' })

@dajaxice_register
def filter_users(request, text):
    result_list = User.objects.filter(username__startswith = text)
    
    uname_list = u''
    
    for usr in list(result_list):
        uname_list += usr.username + ','
    
    return simplejson.dumps({'message': text, 'users':uname_list })

@dajaxice_register
def filter_users_advanced(request, text, min_points):
    #result_list = UserProfile.objects.filter(user_username__contains = text, user_points__gtw = min_points)
    result_list = User.objects.filter(username__contains = text, userprofile__user_points__gte = min_points)
    
    uname_list = u''
    
    for usr in list(result_list):
        uname_list += usr.username + ','
    
    return simplejson.dumps({'message': min_points, 'users':uname_list })

@dajaxice_register
def filter_category_advanced(request, text, min_points, cat):
    #result_list = UserProfile.objects.filter(user_username__contains = text, user_points__gtw = min_points)
    ff_cat_list = Fulfilment.objects.filter(achievement__category_id = cat)
    result_list = User.objects.filter(username__contains = text, userprofile__user_points__gte = min_points)
    
    uname_list = u''
    
    for usr in list(result_list):
	for ff in list(ff_cat_list):
	    if	ff.user == usr:
		uname_list += usr.username + ','
		break
    
    return simplejson.dumps({'message': min_points, 'users':uname_list })  
    
@dajaxice_register
def filter_nocat_advanced(request, text, min_points):
    #result_list = UserProfile.objects.filter(user_username__contains = text, user_points__gtw = min_points)
    ff_nocat_list = Fulfilment.objects.filter(achievement__category = None)
    result_list = User.objects.filter(username__contains = text, userprofile__user_points__gte = min_points)
    
    uname_list = u''
    
    for usr in list(result_list):
	for ff in list(ff_nocat_list):
	    if	ff.user == usr:
		uname_list += usr.username + ','
		break
    
    return simplejson.dumps({'message': min_points, 'users':uname_list })    
    
@dajaxice_register
def filter_friends_advanced(request, text, min_points):
    #result_list = UserProfile.objects.filter(user_username__contains = text, user_points__gtw = min_points)
    result_list = User.objects.filter(username__contains = text, userprofile__user_points__gte = min_points, userprofile__friends__id = request.user.id)
    
    uname_list = u''
    
    for usr in list(result_list):
        uname_list += usr.username + ','
    
    return simplejson.dumps({'message': min_points, 'users':uname_list })

@dajaxice_register
def set_message_read(request, message_id):
    if request.user.is_authenticated():
        message_list = Messages.objects.filter(id = message_id)
        if len(list(message_list)) > 0:
            if message_list[0].message_type != "EM":
                message_list[0].read = True
                message_list[0].save()
                return simplejson.dumps({'success':True, 'id':message_id})
    
    return simplejson.dumps({'success':False, 'id':message_id})
    
@dajaxice_register
def message_remove(request, message_id):
    if request.user.is_authenticated():
        message_list = Messages.objects.filter(id = message_id)
        if len(list(message_list)) > 0:
            message_list[0].hidden = True
            message_list[0].save()
            
            return simplejson.dumps({'success':True, 'id':message_id})
        
    return simplejson.dumps({'success':False, 'id':message_id})


@dajaxice_register
def request_accept(request, message_id):
    if request.user.is_authenticated():
        message_list = Messages.objects.filter(id = message_id, message_type = 'FR')
        if len(list(message_list)) > 0:
            request.user.userprofile.friends.add(message_list[0].author.userprofile)
            request.user.userprofile.save()
            message_list[0].hidden = True
            message_list[0].save()
        return simplejson.dumps({'success':True, 'id':message_id})


@dajaxice_register
def save_profile_changes(request, first_name, last_name, email, description):
    if request.user.is_authenticated():
	request.user.first_name = first_name
	request.user.last_name = last_name
	request.user.email = email	
	request.user.userprofile.description = description
	request.user.save()
	request.user.userprofile.save()
        
        return simplejson.dumps({'success':True, 'first_name':first_name, 'last_name':last_name, 'email':email, 'description':description})